//
//  BaseInfoSummaryCell.h
//  popular
//
//  Created by JOSE VICENTE GARCIA CHAHUAN on 03/01/2019.
//  Copyright © 2019 Everis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdaptativeLabel.h"
#import "AdaptativeButton.h"
#import "DataModelSummary.h"
#import "UILabel+Utils.h"

@class BaseInfoSummaryCell;

static CGFloat const adjustSizeLinkBtn = 15;
static NSInteger const tagForActBtn = 1001;

typedef enum : NSUInteger {
    Min = 0,
    Fix,
    Porcent,
    Total
} FormPaidType;

typedef enum : NSUInteger {
    GotoMovements = 0,
    GotoChangePayMode,
    GotoCreditPay,
} GotoAction;

typedef enum : NSUInteger {
    None = -1,
    ReadyToPay = 0,
    PaidRecipient = 1,
    OtherPaymentsNoReceipt = 2,
    Pending
} ModalShowInfoType;

@protocol BaseInfoSummaryCellProtocol<NSObject>

@optional

-(void)solicitedInfoToShow:(BaseInfoSummaryCell*)cell;
-(void)solicitedGotoMovements;
-(void)solicitedGotoChangePayMode;
-(void)gotoPayDebitFromModal;

@end

@interface BaseInfoSummaryCell : UITableViewCell

@property(weak, nonatomic) IBOutlet AdaptativeLabel* title;
@property(weak, nonatomic) IBOutlet AdaptativeLabel* subtitle;
@property(weak, nonatomic) IBOutlet AdaptativeLabel* import;
@property(weak, nonatomic) IBOutlet UIImageView* iconTitle;
@property(weak, nonatomic) IBOutlet UIImageView* iconSubtitle;
@property(weak, nonatomic) IBOutlet UIImageView* iconAction;
@property(weak, nonatomic) IBOutlet UIView* lineMax;
@property(weak, nonatomic) IBOutlet UIButton* actionBtn;

@property(nonatomic, assign) BOOL isList;
@property(assign, nonatomic) BOOL isShowed;
@property(assign, nonatomic) BOOL isHyperLinkCreated;
@property(assign, nonatomic) GotoAction goTo;
@property(assign, nonatomic) ModalShowInfoType showInfoType;
@property(nonatomic, assign) SummaryDataCell type;
@property(nonatomic, weak) id<BaseInfoSummaryCellProtocol>baseDelegate;


-(void)configure:(NSString *)desc withTitle:(NSString*)title withSubtitle:(NSString*)subtitle withImport:(NSString*)import highLight:(NSArray*)search;
-(void)addHyperLink:(NSString*)tmpSearchString;
-(IBAction)show:(id)sender;
-(IBAction)showInfo:(id)sender;

@end
